package com.shameclock1;

import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.shameclock1.TwitterApp.TwDialogListener;

public class ConnectActivity extends Activity implements OnClickListener
{

	public TwitterApp				mTwitter;

	public static final String		twitter_consumer_key	= "8aYQVP7ikG5Rw0CxAof5eg";
	public static final String		twitter_secret_key		= "JRaN7emY2jMCVx7DzyqGdbKQsCXYlzvJawVuXA2BV1o";

	public boolean					bFacebook;
	public boolean					bTwitter;
	public boolean					bHandset;

	private Session.StatusCallback	statusCallback			= new SessionStatusCallback();

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connect);

		bFacebook = false;
		bTwitter = false;
		bHandset = false;

		mTwitter = new TwitterApp(this, twitter_consumer_key, twitter_secret_key);
		mTwitter.setListener(new TwitterConnectListener());

		if (mTwitter.hasAccessToken())
		{

			bTwitter = true;
			ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
			imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", this.getPackageName()));

			String username = mTwitter.getUsername();
			username = (username.equals("")) ? "Unknown" : username;

			// mTwitterBtn.setText("  Twitter (" + username + ")");
			// mTwitterBtn.setTextColor(Color.WHITE);
		}

		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

		Session session = Session.getActiveSession();
		if (session == null)
		{
			if (savedInstanceState != null)
			{
				session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
			}
			if (session == null)
			{
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED))
			{
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}
		}

		// updateView();

		Display display = getWindowManager().getDefaultDisplay();
		// Point size = new Point();
		// display.getSize(size);

		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;

		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.connectscreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		int height = h * 2 / 10;

		params.height = height;
		params.setMargins(w / 10, 0, w / 10, 0);

		RelativeLayout layout;
		layout = (RelativeLayout) findViewById(R.id.connectrelativelayout1);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		params.width = w;
		params.height = h - height;
		params.setMargins(0, 0, 0, 0);

		layout = (RelativeLayout) findViewById(R.id.connectrelativelayout2);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		height = (h - height) / 11;
		params.height = height;
		params.width = w - w * 2 / 10;
		params.setMargins(w / 10, 0, 0, 0);

		imgView = (ImageView) findViewById(R.id.connectscreenconnect);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = (int) (height * 3 * 1.61);
		params.height = height;

		layout = (RelativeLayout) findViewById(R.id.connectrelativelayout2);
		int width = layout.getLayoutParams().width;

		imgView = (ImageView) findViewById(R.id.connectscreensplitbar);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = width;
		params.height = width / 100;
		params.setMargins(w / 10, 0, 0, 0);

		height = (h - height) / 11;
		width = w - w * 2 / 10;

		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.connectwithfacebook);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = params.width / 4;
		params.setMargins(w / 10, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		int tmp = width / 6;
		if (height < tmp)
			tmp = height;
		params.width = tmp / 2;
		params.height = tmp / 2;
		params.setMargins(w / 10 + width - tmp + tmp / 2, height / 2 + height / 2 - tmp / 4, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.connectwithtwitter);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = params.width / 4;
		params.setMargins(w / 10, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = tmp / 2;
		params.height = tmp / 2;
		params.setMargins(w / 10 + width - tmp + tmp / 2, height / 2 + height / 2 - tmp / 4, 0, 0);

		TextView txtcant = (TextView) findViewById(R.id.connectscreencanthandleit);
		txtcant.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		txtcant.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtcant.getLayoutParams();
		params.width = width * 4 / 6;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.connectscreenposttohandset);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = params.width / 4;
		params.setMargins(w / 10, height / 2 / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.connectscreenposttohandsetcheck);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = tmp / 2;
		params.height = tmp / 2;
		params.setMargins(w / 10 + width - tmp + tmp / 2, height / 2 / 2 + height / 2 - tmp / 4, 0, tmp / 4);

		imgBtn = (ImageButton) findViewById(R.id.connectscreennext);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 4 / 6;
		params.height = height;
		params.setMargins(w / 10, height, 0, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onStart()
	{
		super.onStart();
		Session.getActiveSession().addCallback(statusCallback);
	}

	@Override
	public void onStop()
	{
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	private void updateView()
	{
		Session session = Session.getActiveSession();

		ImageButton imgBtn;

		if (session.isOpened())
		{
			bFacebook = true;
			imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
			imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", this.getPackageName()));

			List<String> permissions = session.getPermissions();
			if (!permissions.contains("publish_actions"))
			{
				Session.NewPermissionsRequest newPermissionRequest = new Session.NewPermissionsRequest(this, Arrays.asList("publish_actions")).setDefaultAudience(SessionDefaultAudience.FRIENDS);
				session.requestNewPublishPermissions(newPermissionRequest);
			}

		}
		else
		{
			bFacebook = false;
			imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
			imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", this.getPackageName()));
		}
	}

	public void onClick(View v)
	{

		final Session session = Session.getActiveSession();

		switch (v.getId())
		{

			case R.id.connectscreenfacebookcheck:

				bFacebook = !bFacebook;
				if (bFacebook)
				{
					if (!session.isOpened())
					{
						AlertDialog.Builder builderInner = new AlertDialog.Builder(ConnectActivity.this);
						builderInner.setCancelable(false);
						builderInner.setTitle("Facebook Login");
						builderInner.setMessage("You must login with facebook.");
						builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								bFacebook = false;
								dialog.dismiss();
							}
						});
						builderInner.show();
					}
					else
					{
						ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
						imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", this.getPackageName()));
					}
				}
				else
				{
					ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
					imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", this.getPackageName()));
				}
				break;

			case R.id.connectscreentwittercheck:

				bTwitter = !bTwitter;
				if (bTwitter)
				{
					if (!mTwitter.hasAccessToken())
					{
						AlertDialog.Builder builderInner = new AlertDialog.Builder(ConnectActivity.this);
						builderInner.setCancelable(false);
						builderInner.setTitle("Twitter Login");
						builderInner.setMessage("You must login with twitter.");
						builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								bTwitter = false;
								dialog.dismiss();
							}
						});
						builderInner.show();
					}
					else
					{
						ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
						imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", this.getPackageName()));
					}
				}
				else
				{
					ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
					imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", this.getPackageName()));
				}
				break;

			case R.id.connectscreenposttohandsetcheck:

				bHandset = !bHandset;

				if (bHandset)
				{
					ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenposttohandsetcheck);
					imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", this.getPackageName()));
				}
				else
				{
					ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenposttohandsetcheck);
					imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", this.getPackageName()));
				}

				break;

			case R.id.connectscreennext:

				if (!bFacebook && !bTwitter && !bHandset)
				{
					AlertDialog.Builder builderInner = new AlertDialog.Builder(ConnectActivity.this);
					builderInner.setCancelable(false);
					builderInner.setMessage("You must choose one poison.");
					builderInner.setTitle("Shame Clock");
					builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							dialog.dismiss();
						}
					});
					builderInner.show();
				}
				else
				{
					Bundle b = new Bundle();
					b.putBoolean("Facebook", bFacebook);
					b.putBoolean("Twitter", bTwitter);
					b.putBoolean("Handset", bHandset);
					Intent activityChangeIntent;
					activityChangeIntent = new Intent(ConnectActivity.this, PostmsgActivity.class);
					activityChangeIntent.putExtras(b);
					ConnectActivity.this.startActivity(activityChangeIntent);
					finish();
				}
				break;
			case R.id.connectscreenposttohandset:

				bHandset = true;
				ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenposttohandsetcheck);
				imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", this.getPackageName()));

				/*
				 * Bundle parameters = new Bundle();
				 * parameters.putString("message", "Text is lame. Listen up:");
				 * if (session != null) {
				 * 
				 * Request request = new Request(session, "me/feed", parameters,
				 * HttpMethod.POST, new Request.Callback() { public void
				 * onCompleted(Response response) { FacebookRequestError error =
				 * response.getError(); if (error != null) { } else { } } });
				 * RequestAsyncTask task = new RequestAsyncTask(request);
				 * task.execute(); }
				 */

				break;
			case R.id.connectwithfacebook:
				if (!session.isOpened() && !session.isClosed())
				{
					session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
				}
				else
				{
					if (session.isClosed())
					{
						Session.openActiveSession(ConnectActivity.this, true, statusCallback);
					}
					else
					{
						final AlertDialog.Builder builder = new AlertDialog.Builder(this);

						builder.setMessage("Delete current Facebook connection?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int id)
							{
								session.closeAndClearTokenInformation();

								bFacebook = false;
								ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
								imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", ConnectActivity.this.getPackageName()));
							}
						}).setNegativeButton("No", new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int id)
							{
								dialog.cancel();

								Session.openActiveSession(ConnectActivity.this, true, statusCallback);

								bFacebook = true;
								ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreenfacebookcheck);
								imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", ConnectActivity.this.getPackageName()));
							}
						});
						final AlertDialog alert = builder.create();

						alert.show();
					}
				}
				break;
			case R.id.connectwithtwitter:
				if (mTwitter.hasAccessToken())
				{
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);

					builder.setMessage("Delete current Twitter connection?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int id)
						{
							mTwitter.resetAccessToken();

							bTwitter = false;
							ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
							imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", ConnectActivity.this.getPackageName()));
						}
					}).setNegativeButton("No", new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int id)
						{
							dialog.cancel();

							bTwitter = true;
							ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
							imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", ConnectActivity.this.getPackageName()));
						}
					});
					final AlertDialog alert = builder.create();

					alert.show();
				}
				else
				{
					bTwitter = false;
					imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
					imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", ConnectActivity.this.getPackageName()));

					mTwitter.authorize();
				}
				break;

			default:
				break;
		}
	}

	private class SessionStatusCallback implements Session.StatusCallback
	{
		@Override
		public void call(Session session, SessionState state, Exception exception)
		{
			updateView();
		}
	}

	public class TwitterConnectListener implements TwDialogListener
	{
		@Override
		public void onComplete(String value)
		{
			String username = mTwitter.getUsername();
			username = (username.equals("")) ? "No Name" : username;

			bTwitter = true;
			ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
			imgBtn.setImageResource(getResources().getIdentifier("connectchecked", "drawable", ConnectActivity.this.getPackageName()));
		}

		@Override
		public void onError(String value)
		{
			bTwitter = false;
			ImageButton imgBtn = (ImageButton) findViewById(R.id.connectscreentwittercheck);
			imgBtn.setImageResource(getResources().getIdentifier("connectunchecked", "drawable", ConnectActivity.this.getPackageName()));
		}
	}
}