package com.shameclock1;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class SetAlarmActivity extends Activity implements OnClickListener
{

	int				intH1;
	int				intH2;
	int				intM1;
	int				intM2;

	public String	strMsg;
	public int		intRepeat;
	public boolean	bRepeatWeekly;
	public int		intSnooze;
	public String	strRingtone;
	public int		intDifficulty;

	public boolean	bFacebook;
	public boolean	bTwitter;
	public boolean	bHandset;

	public boolean 	bW1, bW2, bW3, bW4, bW5, bW6, bW7;

	public CheckBox checkBox1;
	public CheckBox checkBox2;
	public CheckBox checkBox3;
	public CheckBox checkBox4;
	public CheckBox checkBox5;
	public CheckBox checkBox6;
	public CheckBox checkBox7;
	
	Ringtone		ringtone;

	Uri				uriRingtone = Settings.System.DEFAULT_RINGTONE_URI;
	private int		iId = -1;
	private String	strTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setalarm);

		strTitle = "No title";
		strRingtone = uriRingtone.toString();
		
		Bundle b = getIntent().getExtras();

		if (b != null)
		{
			strMsg 		= b.getString("PostMessage");	
			if(strMsg == null) strMsg = "Alarm!";
			
			bFacebook 	= b.getBoolean("Facebook", false);
			bTwitter 	= b.getBoolean("Twitter", false);
			bHandset 	= b.getBoolean("Handset", false);
			iId			= b.getInt("AlarmNumber", -1);
		}
		
		
		SharedPreferences preferences = this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		int hour = 0;
		int minute = 0;
		if(iId != -1)
		{
			strTitle		= preferences.getString(String.format("Title%d", iId), strTitle);
			
			intRepeat 		= preferences.getInt(String.format("Repeat%d", iId), 1);
			intSnooze 		= preferences.getInt(String.format("Snooze%d", iId), 5);
			strRingtone		= preferences.getString(String.format("Ringtone%d", iId), null);
			intDifficulty 	= preferences.getInt(String.format("Difficulty%d", iId), 1);
			strMsg			= preferences.getString(String.format("Message%d", iId), "");
			
			hour 			= preferences.getInt(String.format("H%d", iId), 0); 
			minute 			= preferences.getInt(String.format("M%d", iId), 0); 
			
			if(strRingtone != null && strRingtone.trim().length() != 0)
				uriRingtone		= Uri.parse(strRingtone);
			/*
			
			preferences.getInt(String.format("AH%d", iId), intH1 * 10 + intH2);
			
			preferences.getInt(String.format("M%d", iId), intM1 * 10 + intM2);
			preferences.getInt(String.format("AM%d", iId), intM1 * 10 + intM2);
			*/			
			
			bFacebook		= preferences.getBoolean(String.format("Facebook%d", iId), bFacebook);
			bTwitter 		= preferences.getBoolean(String.format("Twitter%d", iId), bTwitter);
			bHandset 		= preferences.getBoolean(String.format("Handset%d", iId), bHandset);
			
			bW1 			= preferences.getBoolean(String.format("W%d1", iId), false);
			bW2 			= preferences.getBoolean(String.format("W%d2", iId), false);
			bW3 			= preferences.getBoolean(String.format("W%d3", iId), false);
			bW4 			= preferences.getBoolean(String.format("W%d4", iId), false);
			bW5 			= preferences.getBoolean(String.format("W%d5", iId), false);
			bW6 			= preferences.getBoolean(String.format("W%d6", iId), false);
			bW7 			= preferences.getBoolean(String.format("W%d7", iId), false);
			
		}
		else
		{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, 5);
			
			hour = cal.get(Calendar.HOUR_OF_DAY);		
			minute = cal.get(Calendar.MINUTE);

			uriRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

			bRepeatWeekly = true;

			intSnooze = 5;
			intDifficulty = 1;
			intRepeat = 1;

			bW1 = bW2 = bW3 = bW4 = bW5 = bW6 = bW7 = false;
		}
		
		intH1 = hour / 10;
		intH2 = hour % 10;
		intM1 = minute / 10;
		intM2 = minute % 10;;
		
		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;

		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.setalarmscreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		int height = (int) (h * 1.2 / 10);
		params.height = height;
		params.setMargins(w / 10, 0, w / 10, 0);

		RelativeLayout layout;
		layout = (RelativeLayout) findViewById(R.id.setalarmrelativelayout1);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		params.width = w;
		params.height = h - height;

		layout = (RelativeLayout) findViewById(R.id.setalarmrelativelayout2);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		height = (h - height) / 12;
		params.height = height;
		params.width = w - w * 2 / 10;
		params.setMargins(w / 10, 0, 0, 0);

		imgView = (ImageView) findViewById(R.id.setalarmscreenclock);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = height;
		params.height = height;
		params.setMargins(-height / 6, height / 6, 0, height / 6);

		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.setalarmscreensetalarmbtn);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = (int) (height * 3 * 0.97);
		params.height = height;

		layout = (RelativeLayout) findViewById(R.id.setalarmrelativelayout2);
		int width = layout.getLayoutParams().width;

		imgView = (ImageView) findViewById(R.id.setalarmscreensplitbar);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = width;
		params.height = width / 100;
		params.setMargins(w / 10, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.setalarmh1);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		int numberW = width / 6;
		int numberH = (int) (numberW * 1.6);
		int tmpW = (int) (height * 3 * 0.625);
		if (numberW > tmpW)
		{
			numberH = height * 3;
			numberW = tmpW;
		}
		int s = numberW / 10;
		int ss = numberW / 4;
		int allW = numberW * 4 + s * 4 + ss;
		params.width = numberW;
		params.height = numberH;
		params.setMargins(w / 10 + width / 2 - allW / 2, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.setalarmm2);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = numberW;
		params.height = numberH;
		params.setMargins(w / 10 + width / 2 + allW / 2 - numberW, height / 2, 0, 0);

		allW = numberW * 2 + s * 2 + ss;
		imgBtn = (ImageButton) findViewById(R.id.setalarmh2);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = numberW;
		params.height = numberH;
		params.setMargins(w / 10 + width / 2 - allW / 2, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.setalarmm1);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = numberW;
		params.height = numberH;
		params.setMargins(w / 10 + width / 2 + allW / 2 - numberW, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.setalarmdoubledot);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = ss;
		params.height = numberH;
		params.setMargins(w / 10 + width / 2 - ss / 2, height / 2, 0, 0);

		TextView txtView;
		txtView = (TextView) findViewById(R.id.setalarmstrtitle);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);

		EditText editText;
		editText = (EditText) findViewById(R.id.setalarmtitle);
		editText.setText(strTitle);
		params = (RelativeLayout.LayoutParams) editText.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10 + width / 2, height / 2, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrrepeat);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		Button btn;
		btn = (Button) findViewById(R.id.setalarmbtnrepeat);
		btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		btn.setOnClickListener(this);
		btn.setText(getRepeat(intRepeat));
		
		params = (RelativeLayout.LayoutParams) btn.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10 + width / 2, 0, 0, 0);

		int wWeek = width / 7;

		txtView = (TextView) findViewById(R.id.setalarmstrw1);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrw2);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10 + wWeek, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrw3);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10 + wWeek * 2, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrw4);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10 + wWeek * 3, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrw5);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10 + wWeek * 4, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrw6);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10 + wWeek * 5, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrw7);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = wWeek;
		params.height = height / 2;
		params.setMargins(w / 10 + wWeek * 6, 0, 0, 0);

		checkBox1 = (CheckBox) findViewById(R.id.setalarm1);
		checkBox1.setChecked(bW1);
		params = (RelativeLayout.LayoutParams) checkBox1.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		checkBox2 = (CheckBox) findViewById(R.id.setalarm2);
		checkBox2.setChecked(bW2);
		params = (RelativeLayout.LayoutParams) checkBox2.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10 + wWeek, 0, 0, 0);

		checkBox3 = (CheckBox) findViewById(R.id.setalarm3);
		checkBox3.setChecked(bW3);
		params = (RelativeLayout.LayoutParams) checkBox3.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10 + wWeek * 2, 0, 0, 0);

		checkBox4 = (CheckBox) findViewById(R.id.setalarm4);
		checkBox4.setChecked(bW4);
		params = (RelativeLayout.LayoutParams) checkBox4.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10 + wWeek * 3, 0, 0, 0);

		checkBox5 = (CheckBox) findViewById(R.id.setalarm5);
		checkBox5.setChecked(bW5);
		params = (RelativeLayout.LayoutParams) checkBox5.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10 + wWeek * 4, 0, 0, 0);

		checkBox6 = (CheckBox) findViewById(R.id.setalarm6);		
		checkBox6.setChecked(bW6);
		params = (RelativeLayout.LayoutParams) checkBox6.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10 + wWeek * 5, 0, 0, 0);

		checkBox7 = (CheckBox) findViewById(R.id.setalarm7);
		checkBox7.setChecked(bW7);
		params = (RelativeLayout.LayoutParams) checkBox7.getLayoutParams();
		params.width = wWeek;
		params.height = height;
		params.setMargins(w / 10 + wWeek * 6, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrsnooze);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);

		btn = (Button) findViewById(R.id.setalarmbtnsnooze);
		btn.setOnClickListener(this);
		btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		btn.setText(String.format("%d min", intSnooze));
		params = (RelativeLayout.LayoutParams) btn.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10 + width / 2, height / 2, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrsound);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		btn = (Button) findViewById(R.id.setalarmbtnsound);
		btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		btn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) btn.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10 + width / 2, 0, 0, 0);

		txtView = (TextView) findViewById(R.id.setalarmstrdifficulty);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		btn = (Button) findViewById(R.id.setalarmbtndifficulty);
		btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, height / 3);
		btn.setOnClickListener(this);
		btn.setText(getDifficulty(intDifficulty));
		params = (RelativeLayout.LayoutParams) btn.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10 + width / 2, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.setalarmbtncancel);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.setalarmbtndone);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10 + width / 2, 0, 0, 0);

		this.setNewTime();

		showRepeat(intRepeat);
	}

	@Override
	public void onResume()
	{		
		super.onResume();
		
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(checkBox1.getWindowToken(), 0);		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void showFileChooser()
	{

		Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
		if (uriRingtone != null)
		{
			intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, uriRingtone);
			intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, uriRingtone);
		}
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == RESULT_OK)
		{
			switch (requestCode)
			{
				case 1:
					
					uriRingtone = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

					if(uriRingtone != null)
						strRingtone = uriRingtone.toString();
					else
						strRingtone = "";

					// ringtone =
					// RingtoneManager.getRingtone(getApplicationContext(),
					// uri);
					// ringtone.play();
					break;

				default:
					break;
			}
		}
	}

	public void onClick(View v)
	{
//		Intent in1 = new Intent(SetAlarmActivity.this, AddAlarmActivity.class);
		Button btn;
		switch (v.getId())
		{
			case R.id.setalarmbtndone:

				saveAlarm();
				
				break;
			case R.id.setalarmbtnrepeat:

				intRepeat++;
				if (intRepeat > 2)
					intRepeat = 1;
				
				btn = (Button) findViewById(R.id.setalarmbtnrepeat);
				btn.setText(getRepeat(intRepeat));
				
				showRepeat(intRepeat);
				break;
			case R.id.setalarmbtnsnooze:
				intSnooze += 5;
				if (intSnooze > 30)
					intSnooze = 5;
				btn = (Button) findViewById(R.id.setalarmbtnsnooze);
				btn.setText(String.format("%d min", intSnooze));
				break;
			case R.id.setalarmbtnsound:

				this.showFileChooser();

				break;
			case R.id.setalarmbtndifficulty:

				intDifficulty++;
				if (intDifficulty > 3)
					intDifficulty = 1;
				
				Button btn1 = (Button) findViewById(R.id.setalarmbtndifficulty);
				btn1.setText(getDifficulty(intDifficulty));
				break;
			case R.id.setalarmh1:
				incHour();
				break;
			case R.id.setalarmh2:
				incHour();
				break;
			case R.id.setalarmm1:
				intM1++;
				if (intM1 > 5)
					intM1 = 0;
				break;
			case R.id.setalarmm2:
				intM2++;
				if (intM2 > 9)
					intM2 = 0;
				break;
			case R.id.setalarmbtncancel:
				finish();
				break;
			default:
				break;
		}
		this.setNewTime();
	}

	private void incH1()
	{
		intH1++;
		if (intH2 > 3)
		{
			if (intH1 > 1)
				intH1 = 0;
		}
		else
		{
			if (intH1 > 2)
				intH1 = 0;
		}
	}

	private void incH2()
	{
		intH2++;
		if (intH1 == 2)
		{
			if (intH2 > 3)
				intH2 = 0;
		}
		else
		{
			if (intH2 > 9)
				intH2 = 0;
		}
	}

	private void incHour()
	{
		intH2++;
		if(intH2 > 3 && intH1 == 2)
		{
			intH1 = 0;
			intH2 = 0;
		}
		else if (intH2 > 9)
		{
			intH2 = 0;
			intH1++;
		}		
	}

	public void setNewTime()
	{
		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.setalarmh1);
		imgBtn.setImageResource(getResources().getIdentifier(String.format("n%d", intH1), "drawable", this.getPackageName()));
		imgBtn = (ImageButton) findViewById(R.id.setalarmh2);
		imgBtn.setImageResource(getResources().getIdentifier(String.format("n%d", intH2), "drawable", this.getPackageName()));
		imgBtn = (ImageButton) findViewById(R.id.setalarmm1);
		imgBtn.setImageResource(getResources().getIdentifier(String.format("n%d", intM1), "drawable", this.getPackageName()));
		imgBtn = (ImageButton) findViewById(R.id.setalarmm2);
		imgBtn.setImageResource(getResources().getIdentifier(String.format("n%d", intM2), "drawable", this.getPackageName()));
	}
	
	public String getDifficulty(int diff)
	{
		if (diff == 2)
			return "Medium";
		else if (diff == 3)
			return "Hard";
		else
			return "Easy";
	}
	
	public String getRepeat(int rep)
	{
		if(rep == 2)
			return "Weekly";
		else //if(rep == 1)
			return "One Time";								
	}

	private void showRepeat(int rep)
	{
		switch (rep)
		{
			case 2:
				checkBox1.setEnabled(true);
				checkBox2.setEnabled(true);
				checkBox3.setEnabled(true);
				checkBox4.setEnabled(true);
				checkBox5.setEnabled(true);
				checkBox6.setEnabled(true);
				checkBox7.setEnabled(true);
				break;
			case 1:
				checkBox1.setEnabled(false);
				checkBox2.setEnabled(false);
				checkBox3.setEnabled(false);
				checkBox4.setEnabled(false);
				checkBox5.setEnabled(false);
				checkBox6.setEnabled(false);
				checkBox7.setEnabled(false);
				break;
			default:
				break;
		}
	}

	private void saveAlarm()
	{
		EditText editText = (EditText) findViewById(R.id.setalarmtitle);
	
	
		String strKey = "AlarmCnt";
		SharedPreferences preferences = this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		
		if(iId == -1)
		{
			iId = preferences.getInt(strKey, 0);
			iId++;
		}
		
		editor.putInt(strKey, iId);				
		editor.putString(String.format("Title%d", iId), editText.getText().toString());
		editor.putInt(String.format("Repeat%d", iId), intRepeat);
		editor.putInt(String.format("Snooze%d", iId), intSnooze);
		editor.putString(String.format("Ringtone%d", iId), strRingtone);
		editor.putInt(String.format("Difficulty%d", iId), intDifficulty);
		editor.putString(String.format("Message%d", iId), strMsg);
		editor.putBoolean(String.format("On%d", iId), true);
		editor.putInt(String.format("H%d", iId), intH1 * 10 + intH2);
		editor.putInt(String.format("AH%d", iId), intH1 * 10 + intH2);
		editor.putInt(String.format("M%d", iId), intM1 * 10 + intM2);
		editor.putInt(String.format("AM%d", iId), intM1 * 10 + intM2);
		editor.putBoolean(String.format("Facebook%d", iId), bFacebook);
		editor.putBoolean(String.format("Twitter%d", iId), bTwitter);
		editor.putBoolean(String.format("Handset%d", iId), bHandset);
		editor.putBoolean(String.format("Active%d", iId), true);		
	
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String str1 = sdf.format(new Date());
		String[] tmpStr = str1.split(":");
		int tmpH = 0;
		int tmpM = 0;
		try
		{
			tmpH = Integer.parseInt(tmpStr[0]);
			tmpM = Integer.parseInt(tmpStr[1]);
		}
		catch (Exception e)
		{
	
		}
	
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		if (intH1 * 10 + intH2 < tmpH)
		{
			day++;
		}
		else if (intH1 * 10 + intH2 == tmpH)
		{
			if (intM1 * 10 + intM2 <= tmpM)
			{
				day++;
			}
		}
		if (day > 7)
			day = 1;
	
		bW1 = checkBox1.isChecked();
		bW2 = checkBox2.isChecked();
		bW3 = checkBox3.isChecked();
		bW4 = checkBox4.isChecked();
		bW5 = checkBox5.isChecked();
		bW6 = checkBox6.isChecked();
		bW7 = checkBox7.isChecked();
	
		editor.putBoolean(String.format("W%d1", iId), bW1);
		editor.putBoolean(String.format("W%d2", iId), bW2);
		editor.putBoolean(String.format("W%d3", iId), bW3);
		editor.putBoolean(String.format("W%d4", iId), bW4);
		editor.putBoolean(String.format("W%d5", iId), bW5);
		editor.putBoolean(String.format("W%d6", iId), bW6);
		editor.putBoolean(String.format("W%d7", iId), bW7);
		
		int AW = -1;
	
		for (int i = day - 1; i >= 1; i--)
		{
			boolean bW = preferences.getBoolean(String.format("W%d%d", iId, i), false);
			if (bW)
			{
				AW = i;
			}
		}
		for (int i = 7; i >= day; i--)
		{
			boolean bW = preferences.getBoolean(String.format("W%d%d", iId, i), false);
			if (bW)
			{
				AW = i;
			}
		}
		editor.putInt(String.format("AW%d", iId), AW);
		editor.commit();
	
		finish();
	}
	

}