package com.shameclock1;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AppEventsLogger;

@SuppressLint("NewApi")
public class MainActivity extends Activity
{
	private static final long	SPLASH_DELAY	= 3000;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

//		keyHash();

		setContentView(R.layout.activity_main);

		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;

		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.mainscreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		params.height = h * 2 / 10;
		params.setMargins(w / 10, 0, w / 10, 0);

		imgView = (ImageView) findViewById(R.id.mainscreenicon);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		params.height = h * 10 / 10;
		params.setMargins(w / 10, 0, w / 10, 0);

		TextView textViewProof = (TextView) findViewById(R.id.mainscreenfoolproof);
		textViewProof.setTextSize(TypedValue.COMPLEX_UNIT_PX, h / 25);
		params = (RelativeLayout.LayoutParams) textViewProof.getLayoutParams();
		params.width = w;
		params.height = h * 2 / 10;
		params.setMargins(w / 10, 0, w / 10, 0);

		startIntro();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		AppEventsLogger.activateApp(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void startIntro()
	{
		CountDownTimer timer = new CountDownTimer(SPLASH_DELAY, 100)
		{
			@Override
			public void onFinish()
			{
				startApp();
			}

			@Override
			public void onTick(long millisUntilFinished)
			{
			}
		};
		timer.start();
	}

	public void startApp()
	{
		AddAlarmActivity.wasShowSplash = true;
		
		Bundle bundle = getIntent().getExtras();
		if(bundle != null && !bundle.getBoolean("first_start")) {
			finish();
			return;
		}
		
		Intent activityChangeIntent;
		activityChangeIntent = new Intent(MainActivity.this, AddAlarmActivity.class);
		// activityChangeIntent = new Intent(MainActivity.this,
		// CalcActivity.class);

		// activityChangeIntent = new Intent(MainActivity.this,
		// ConnectActivity.class);
		// activityChangeIntent = new Intent(MainActivity.this,
		// LoginActivity.class);

		// activityChangeIntent = new Intent(MainActivity.this,
		// PostmsgActivity.class);

		// activityChangeIntent = new Intent(MainActivity.this,
		// SetAlarmActivity.class);

		// activityChangeIntent = new Intent(MainActivity.this,
		// SnoozeActivity.class);
		// int value = 1;
		// activityChangeIntent.putExtra("AlarmNumber", value);

		startActivity(activityChangeIntent);

		finish();
	}

	private void keyHash()
	{
		// Add code to print out the key hash
		try
		{
			PackageInfo info = getPackageManager().getPackageInfo("com.shameclock", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures)
			{
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
				Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
				Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		}
		catch (NameNotFoundException e)
		{

		}
		catch (NoSuchAlgorithmException e)
		{

		}
	}
}
