package com.shameclock1;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class CalcActivity extends Activity implements OnClickListener
{

	ArrayList<Alarm>	alarms;

	public String		quiz;
	public int			intAnswer;
	public int			intDifficulty;
	public int			intRights;
	public int			intWrongs;
	
	//public Intent		activityChangeIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calc);

		intRights = 0;
		intWrongs = 0;
		
		//activityChangeIntent = new Intent(CalcActivity.this, AddAlarmActivity.class);

		Bundle b = getIntent().getExtras();
		int intAlarmNumber = b.getInt("AlarmNumber");
		SharedPreferences preferences = this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		intDifficulty = preferences.getInt(String.format("Difficulty%d", intAlarmNumber), 1);

		makeQuiz(1);

		Display display = getWindowManager().getDefaultDisplay();
		// Point size = new Point();
		// display.getSize(size);

		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;

		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.calcscreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		int height = h * 2 / 10;
		params.height = height;
		params.setMargins(w / 10, 0, w / 10, 0);

		RelativeLayout layout;
		layout = (RelativeLayout) findViewById(R.id.calcrelativelayout1);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		params.width = w;
		params.height = h - height;

		layout = (RelativeLayout) findViewById(R.id.calcrelativelayout2);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		height = (h - height) / 11;
		params.height = height;
		params.width = w - w * 2 / 10;
		params.setMargins(w / 10, 0, 0, 0);

		imgView = (ImageView) findViewById(R.id.calcscreenclock);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = height;
		params.height = height;
		params.setMargins(-height / 6, height / 6, 0, height / 6);

		layout = (RelativeLayout) findViewById(R.id.calcrelativelayout2);
		int width = layout.getLayoutParams().width;

		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.calcscreenaddalarmbtn);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		int tmp = (int) (height * 3 * 1.8);
		if (tmp > width)
			tmp = width;
		params.width = tmp;
		params.height = height;

		imgView = (ImageView) findViewById(R.id.calcscreensplitbar);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = width;
		params.height = width / 100;
		params.setMargins(w / 10, 0, 0, 0);

		TextView txtView;
		txtView = (TextView) findViewById(R.id.calcscreenproblem);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (height * 0.6));
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);

		txtView = (TextView) findViewById(R.id.calcscreenanswer);
		txtView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (height * 0.6));
		params = (RelativeLayout.LayoutParams) txtView.getLayoutParams();
		params.width = width / 2;
		params.height = height;
		params.setMargins(0, height / 2, 0, 0);

		int space = width / 3 / 10;
		int keyW = (width - (space * 2)) / 3;

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard1);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard2);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width / 2 - keyW / 2, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard3);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width - keyW, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard4);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard5);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width / 2 - keyW / 2, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard6);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width - keyW, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard7);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard8);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width / 2 - keyW / 2, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard9);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width - keyW, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboardr);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboard0);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width / 2 - keyW / 2, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboardb);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width - keyW, 0, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calckeyboardm);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10, 0, 0, 0);

		/*
		 * imgBtn = (ImageButton)findViewById(R.id.calckeyboarde);
		 * imgBtn.setOnClickListener(this); params =
		 * (RelativeLayout.LayoutParams)imgBtn.getLayoutParams(); params.width =
		 * keyW * 2 + space; params.height = height;
		 * params.setMargins(w/10+keyW+space, 0, 0, 0);
		 */

		imgBtn = (ImageButton) findViewById(R.id.calcsnooze);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		keyW = (width - space) / 2;
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);

		imgBtn = (ImageButton) findViewById(R.id.calcdisable);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = keyW;
		params.height = height;
		params.setMargins(w / 10 + width - keyW, height / 2, 0, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	public void onClick(View v)
	{
		TextView txtView = (TextView) findViewById(R.id.calcscreenanswer);
		String str = txtView.getText().toString();

		switch (v.getId())
		{
			case R.id.calckeyboard0:
				str += "0";
				break;
			case R.id.calckeyboard1:
				str += "1";
				break;
			case R.id.calckeyboard2:
				str += "2";
				break;
			case R.id.calckeyboard3:
				str += "3";
				break;
			case R.id.calckeyboard4:
				str += "4";
				break;
			case R.id.calckeyboard5:
				str += "5";
				break;
			case R.id.calckeyboard6:
				str += "6";
				break;
			case R.id.calckeyboard7:
				str += "7";
				break;
			case R.id.calckeyboard8:
				str += "8";
				break;
			case R.id.calckeyboard9:
				str += "9";
				break;
			case R.id.calckeyboardr:
				str = "";
				break;
			case R.id.calckeyboardb:
				if (str.length() > 0)
					str = str.substring(0, str.length() - 1);
				break;
			// case R.id.calckeyboarde:
			// break;
			case R.id.calckeyboardm:
				SnoozeActivity.me.mMediaPlayer.stop();
				break;
			case R.id.calcsnooze:
				SnoozeActivity.me.snooze();
				finish();
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
				break;
			case R.id.calcdisable:
				
				int a = Utils.getInt(str);
				
/*						
				(1R 0W) > (2R 0W) > Disable
				(0R 1W) > (0R 2W) > Post made
				
				(1R 0W) > (1R 1W) > (2R 1W) > Disable
				(0R 1W) > (1R 1W) > (2R 1W) > Disable
				
				(0R 1W) > (1R 1W) > (1R 2W) > Post made
				(1R 0W) > (1R 1W) > (1R 2W) > Post made 
				
*/						
				if (intAnswer == a)
				{
					if(intRights == 0)
					{
						intRights = 1;
						right();
					}
					else if(intRights == 1)
					{						
						disable();	
					}
				}
				else
				{
					if(intWrongs == 0)
					{
						intWrongs = 1;
						wrong();
					}
					else if(intWrongs == 1)
					{
						intWrongs = 2;
						post();	
					}
				}
				break;
			default:
				break;
		}
		txtView.setText(str);
	}

	@Override
	public void onBackPressed()
	{
		finish();
		Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
		return;
	}

	public void makeQuiz(int count)
	{
		ImageView imgView = (ImageView) findViewById(R.id.calcscreenaddalarmbtn);
		imgView.setImageResource(getResources().getIdentifier(String.format("problem%doutof2", count), "drawable", this.getPackageName()));

		int num1 = 0;
		int num2 = 0;

		int tmp;
		for (int i = 0; i < intDifficulty; i++)
		{
			tmp = (int) (Math.random() * 10);
			tmp = (tmp % 9) + 1;
			num1 += tmp * (int) (Math.pow(10, i));

			tmp = (int) (Math.random() * 10);
			tmp = (tmp % 9) + 1;
			num2 += tmp * (int) (Math.pow(10, i));
		}

		if (num1 < num2)
		{
			tmp = num1;
			num1 = num2;
			num2 = tmp;
		}

		tmp = (int) (Math.random() * 10);
		tmp = tmp % 2;

		if (tmp == 1)
		{
			quiz = String.format("%d+%d=", num1, num2);
			if (intDifficulty < 3)
				quiz = String.format("%d + %d = ", num1, num2);
			intAnswer = num1 + num2;
		}
		else
		{
			quiz = String.format("%d-%d=", num1, num2);
			if (intDifficulty < 3)
				quiz = String.format("%d - %d = ", num1, num2);
			intAnswer = num1 - num2;
		}
		TextView txtView = (TextView) findViewById(R.id.calcscreenproblem);
		txtView.setText(quiz);
		txtView = (TextView) findViewById(R.id.calcscreenanswer);
		txtView.setText("");
	}

	public void right()
	{
		AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(CalcActivity.this);
		dlgBuilder.setCancelable(false);
		dlgBuilder.setIcon(R.drawable.ic_launcher);
		dlgBuilder.setTitle("Correct!");
		dlgBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton)
			{
				makeQuiz(2);
			}
		});

		dlgBuilder.show();		
	}
	
	public void wrong()
	{
		AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(CalcActivity.this);
		dlgBuilder.setCancelable(false);
		dlgBuilder.setIcon(R.drawable.ic_launcher);
		dlgBuilder.setTitle("Incorrect! one more chance");
		dlgBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton)
			{
				makeQuiz(2);
			}
		});

		dlgBuilder.show();	
	}
	
	public void disable()
	{
		SnoozeActivity.me.disable();
	
		AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(CalcActivity.this);
		dlgBuilder.setCancelable(false);
		dlgBuilder.setIcon(R.drawable.ic_launcher);
		dlgBuilder.setTitle("Correct! Alarm disabled.");
		dlgBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton)
			{
				SnoozeActivity.me.disable();				
				finish();
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
			}
		});
	
		dlgBuilder.show();		
	}

	public void post()
	{
		AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(CalcActivity.this);
		dlgBuilder.setCancelable(false);
		dlgBuilder.setIcon(R.drawable.ic_launcher);
		dlgBuilder.setTitle("Incorrect!");
		dlgBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton)
			{
				SnoozeActivity.me.post();	
				finish();
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
			}
		});
	
		dlgBuilder.show();		
	}
}