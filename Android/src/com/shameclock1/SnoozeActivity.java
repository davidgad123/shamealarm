package com.shameclock1;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;

public class SnoozeActivity extends Activity implements OnClickListener
{

	public static final String	TAG	= "SnoozeActivity";
	
	public static final int	POST_DELAY	= 60 * 1000;
	CountDownTimer timer;
	
	ArrayList<Alarm>				alarms;

	int								intAlarmNumber;

	Ringtone						ringtone;
	MediaPlayer						mMediaPlayer;

	private TextView 				textDownTimeView;
	
	public String					strMsg;

	public boolean					bFacebook;
	public boolean					bTwitter;
	public boolean					bHandset;

	public static SnoozeActivity	me;

	public TwitterApp				mTwitter;

	public static final String		twitter_consumer_key	= "8aYQVP7ikG5Rw0CxAof5eg";
	public static final String		twitter_secret_key		= "JRaN7emY2jMCVx7DzyqGdbKQsCXYlzvJawVuXA2BV1o";


	public Session					session;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_snooze);
	
		session = Session.getActiveSession();
		if (session == null)
		{
			if (savedInstanceState != null)
			{
				session = Session.restoreSession(this, null, null, savedInstanceState);
			}
			if (session == null)
			{
				session = new Session(this);
			}
			
//			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED))
			{
				session.openForRead(new Session.OpenRequest(this).setCallback(null));
			}
		}		
		
		
		
		
	
		mTwitter = new TwitterApp(this, twitter_consumer_key, twitter_secret_key);
	
		me = this;
	
		Bundle b = getIntent().getExtras();
		intAlarmNumber = b.getInt("AlarmNumber");
	
		SharedPreferences preferences = this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		strMsg = preferences.getString(String.format("Message%d", intAlarmNumber), "Wake me up!");
		String strRingtone = preferences.getString(String.format("Ringtone%d", intAlarmNumber), "Default ringtone");
		String title = preferences.getString(String.format("Title%d", intAlarmNumber), "Alarm");
		bFacebook = preferences.getBoolean(String.format("Facebook%d", intAlarmNumber), false);
		bTwitter = preferences.getBoolean(String.format("Twitter%d", intAlarmNumber), false);
		bHandset = preferences.getBoolean(String.format("Handset%d", intAlarmNumber), false);
	
		Uri uri = Uri.parse(strRingtone);
	
		/*
		 * try { ringtone = RingtoneManager.getRingtone(getApplicationContext(),
		 * uri); ringtone.play(); } catch (Exception e) {
		 * 
		 * }
		 */
	
		try
		{
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
			mMediaPlayer.setVolume(0.8f, 0.8f);
			mMediaPlayer.setDataSource(this, uri);
			final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			
			int maxAlram = audioManager.getStreamMaxVolume (AudioManager.STREAM_ALARM);
			audioManager.setStreamVolume(AudioManager.STREAM_ALARM, maxAlram, 0);
			
//			if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0)
//			{
//				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
				mMediaPlayer.setLooping(true);
				mMediaPlayer.prepare();
				mMediaPlayer.start();
//			}
		}
		catch (Exception e)
		{
		}
	
		Display display = getWindowManager().getDefaultDisplay();
		// Point size = new Point();
		// display.getSize(size);
	
		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;
	
		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.snoozescreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		int height = h * 1 / 3;
		params.height = height;
		params.setMargins(w / 10, height / 6, w / 10, 0);
	
		RelativeLayout layout;
		layout = (RelativeLayout) findViewById(R.id.snoozerelativelayout1);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		params.width = w;
		params.height = h - height;
	
		height = (h - height) / 8;
	
		int width = w - w * 2 / 10;
	
		TextView textView = (TextView) findViewById(R.id.snoozescreentexttitle);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, h / 30);
		params = (RelativeLayout.LayoutParams) textView.getLayoutParams();
		params.width = width;
		params.height = (int) (height * 1.2);
		params.setMargins(w / 10, 0, 0, 0);

		textDownTimeView = (TextView) findViewById(R.id.snoozescreentextdowntime);
		textDownTimeView.setTextSize(TypedValue.COMPLEX_UNIT_PX, h / 15);
		params = (RelativeLayout.LayoutParams) textDownTimeView.getLayoutParams();
		params.width = width;
		params.height = (int) (height * 1.2);
		params.setMargins(w / 10, 0, 0, 0);

		textView = (TextView) findViewById(R.id.snoozescreenmsg);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, h / 30);
		textView.setText(strMsg);
		params = (RelativeLayout.LayoutParams) textView.getLayoutParams();
		params.width = width;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);
	
		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.snoozescreensnooze);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);
	
		imgBtn = (ImageButton) findViewById(R.id.snoozescreendisable);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width;
		params.height = height;
		params.setMargins(w / 10, height / 4, 0, 0);
	
		unlockScreen();
		
		startDownTimer();
	}

	private void startDownTimer() {
		timer = new CountDownTimer(POST_DELAY, 1000) {
			@Override
			public void onFinish() {
				post();
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
			}

			@Override
			public void onTick(long millisUntilFinished) {
				textDownTimeView.setText("" + (millisUntilFinished / 1000 - 1));
			}
		};
		timer.start();
	}
	
	@Override
	protected void onDestroy() {
		timer.cancel();
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
	
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed()
	{
		this.snooze();
		finish();
		Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
		return;
	}

	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.snoozescreensnooze:
				snooze();
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
				break;
			case R.id.snoozescreendisable:
				Bundle b = new Bundle();
				b.putInt("AlarmNumber", intAlarmNumber);
	
				Intent activityChangeIntent;
				activityChangeIntent = new Intent(SnoozeActivity.this, CalcActivity.class);
				activityChangeIntent.putExtras(b);
				startActivity(activityChangeIntent);
				break;
			default:
				break;
		}
	}

	public void snooze()
	{
		AddAlarmActivity.bBeingAlarm = false;
		stopMedia();

		finish();
	}
	
	public void post()
	{
		boolean fin = false;
		stopMedia();

		disableAlarm();
		
		if (bFacebook)
		{
			Toast.makeText(this, "Posting to Facebook...", Toast.LENGTH_LONG).show();
			
			Bundle parameters = new Bundle();
			parameters.putString("message", strMsg);
			if (session != null)
			{
				Request request = new Request(session, "me/feed", parameters, HttpMethod.POST, new Request.Callback()
				{
					public void onCompleted(Response response)
					{
						FacebookRequestError error = response.getError();
						if (error != null)
						{
							Log.i(TAG, "Facebook error: " + error.toString());
						}
						else
						{
						}
						finishSnooze();
					}
				});
				RequestAsyncTask task = new RequestAsyncTask(request);
				task.execute();
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
			}
			else
			{
				fin = true;
			}
		}

		if (bTwitter)
		{
			Toast.makeText(this, "Posting to Twitter...", Toast.LENGTH_LONG).show();
			
			fin = false;
			new Thread()
			{
				@Override
				public void run()
				{
					try
					{
						mTwitter.updateStatus(strMsg);
					}
					catch (Exception e)
					{
					}
					
					finishSnooze();
				}
			}.start();
		}
		
		if(fin) finish();
	}

	public void disable()
	{
		disableAlarm();
		
		stopMedia();
		finish();
		Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
	}
	
	public void disableAlarm()
	{
		SharedPreferences preferences = this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		int repeat = preferences.getInt(String.format("Repeat%d", intAlarmNumber), 0);
		if (repeat == 1)
		{
			editor.putBoolean(String.format("On%d", intAlarmNumber), false);
		}
		else if (repeat == 2)
		{
			int ahh = preferences.getInt(String.format("H%d", intAlarmNumber), 0);
			int amm = preferences.getInt(String.format("M%d", intAlarmNumber), 0);
			editor.putInt(String.format("AH%d", intAlarmNumber), ahh);
			editor.putInt(String.format("AM%d", intAlarmNumber), amm);
	
			Calendar calendar = Calendar.getInstance();
			int day = calendar.get(Calendar.DAY_OF_WEEK);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String str1 = sdf.format(new Date());
			String[] tmpStr = str1.split(":");
			int tmpH = 0;
			int tmpM = 0;
			try
			{
				tmpH = Integer.parseInt(tmpStr[0]);
				tmpM = Integer.parseInt(tmpStr[1]);
			}
			catch (Exception e)
			{
	
			}
			if (ahh < tmpH)
			{
				day++;
			}
			else if (ahh == tmpH)
			{
				if (amm <= tmpM)
				{
					day++;
				}
			}
			if (day > 7)
				day = 1;
	
			int AW = -1;
	
			for (int i = day - 1; i >= 1; i--)
			{
				boolean bW = preferences.getBoolean(String.format("W%d%d", intAlarmNumber, i), false);
				if (bW)
				{
					AW = i;
				}
			}
			for (int i = 7; i >= day; i--)
			{
				boolean bW = preferences.getBoolean(String.format("W%d%d", intAlarmNumber, i), false);
				if (bW)
				{
					AW = i;
				}
			}
			editor.putInt(String.format("AW%d", intAlarmNumber), AW);
		}
		AddAlarmActivity.bBeingAlarm = false;
		editor.commit();
	}

	public void stopMedia()
	{
		try
		{
			// ringtone.stop();
			mMediaPlayer.stop();
		}
		catch (Exception e)
		{

		}		
	}
	
	public void finishSnooze()
	{
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				AddAlarmActivity.bBeingAlarm = false;
				Utils.g_bPreActivityFlag = Utils.INTERNAL_ACTIVITY;
				finish();
			}			
		});
	}
	
	private void unlockScreen() 
	{
        Window window = this.getWindow();
        window.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
    }	
}