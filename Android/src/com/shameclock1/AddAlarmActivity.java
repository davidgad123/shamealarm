package com.shameclock1;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

@SuppressLint("NewApi")
public class AddAlarmActivity extends Activity implements OnClickListener
{

	ArrayList<Alarm>		alarms;
	public Timer			myTimer;

	public static boolean	bBeingAlarm;

	public static boolean wasShowSplash = false;
	public static boolean wasMinimize = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addalarm);

		bBeingAlarm = false;

		Point size = Utils.getScreenSize(this);
		
		int w = size.x;
		int h = size.y;

		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.addalarmscreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		int height = h * 2 / 10;
		params.height = height;
		params.setMargins(w / 10, 0, w / 10, 0);

		RelativeLayout layout;
		layout = (RelativeLayout) findViewById(R.id.addalarmrelativelayout1);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		params.width = w;
		params.height = h - height;

		layout = (RelativeLayout) findViewById(R.id.addalarmrelativelayout2);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		height = (h - height) / 11;
		params.height = height;
		params.width = w - w * 2 / 10;
		params.setMargins(w / 10, 0, 0, 0);

		imgView = (ImageView) findViewById(R.id.addalarmscreenclock);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = height;
		params.height = height;
		params.setMargins(-height / 6, height / 6, 0, height / 6);

		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.addalarmscreenaddalarmbtn);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = height * 3;
		params.height = height;

		imgBtn = (ImageButton) findViewById(R.id.addalarmscreenaddalarmbtn1);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = height;
		params.height = height;
		layout = (RelativeLayout) findViewById(R.id.addalarmrelativelayout2);
		int width = layout.getLayoutParams().width;
		params.setMargins(width - height + height * 2 / 5, height / 5, 0, height / 5);
		imgBtn.setLayoutParams(params);

		imgView = (ImageView) findViewById(R.id.addalarmscreensplitbar);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = width;
		params.height = width / 100;
		params.setMargins(w / 10, 0, 0, 0);

		ScrollView scrollView = (ScrollView) findViewById(R.id.addalarmscreenscrollview);
		layout = (RelativeLayout) findViewById(R.id.addalarmrelativelayout3);

		RelativeLayout layout1 = (RelativeLayout) findViewById(R.id.addalarmrelativelayout2);

		params = (RelativeLayout.LayoutParams) scrollView.getLayoutParams();
		params.height = layout1.getLayoutParams().height - imgView.getLayoutParams().height - height;
		params.width = w - w * 2 / 10;
		params.setMargins(w / 10, 0, 0, 0);

		alarms = new ArrayList<Alarm>();

		setupTimer();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		if(!wasShowSplash && wasMinimize && Utils.g_bPreActivityFlag == Utils.EXTERNAL_ACTIVTY) {
			Intent intentSplash = new Intent(this, MainActivity.class);
			Bundle bundle = new Bundle();
			bundle.putBoolean("first_start", false);
			intentSplash.putExtras(bundle);
			startActivity(intentSplash);
		} else {
			wasShowSplash = false;
			wasMinimize = true;
		}
		
		if (Utils.g_bPreActivityFlag != Utils.EXTERNAL_ACTIVTY) {
			Utils.g_bPreActivityFlag = Utils.EXTERNAL_ACTIVTY;
		}
		
		load();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
	
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClick(View v)
	{
	
		switch (v.getId())
		{
			case R.id.addalarmscreenaddalarmbtn1:
				
				wasMinimize = false;
				
				Intent activityChangeIntent;
				activityChangeIntent = new Intent(AddAlarmActivity.this, ConnectActivity.class);
				AddAlarmActivity.this.startActivity(activityChangeIntent);
				break;
			default:
				break;
		}
	}

	public void setupTimer()
	{
		myTimer = new Timer();
		myTimer.scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run()
			{
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						setupAlarms();						
					}
				});
			}
		}, 0, 1000);
	}

	public void setupAlarms()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String str1 = sdf.format(new Date());
		String[] tmp = str1.split(":");
		int tmpH = 0;
		int tmpM = 0;
		try
		{
			tmpH = Integer.parseInt(tmp[0]);
			tmpM = Integer.parseInt(tmp[1]);
		}
		catch (Exception e)
		{

		}
		
		System.out.println(tmp[0] + "," + tmp[1] + "," + tmp[2]);
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		SharedPreferences preferences = AddAlarmActivity.this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		String strKey = "AlarmCnt";
		int n = preferences.getInt(strKey, 0);

		for (int i = 0; i < n; i++)
		{
			boolean active = preferences.getBoolean(String.format("Active%d", i + 1), false);
			if(!active) continue;
			
			boolean on = preferences.getBoolean(String.format("On%d", i + 1), false);
			int ahh = preferences.getInt(String.format("AH%d", i + 1), 0);
			int amm = preferences.getInt(String.format("AM%d", i + 1), 0);
			int repeat = preferences.getInt(String.format("Repeat%d", i + 1), 0);
			int snooze = preferences.getInt(String.format("Snooze%d", i + 1), 0);
			int AW = preferences.getInt(String.format("AW%d", i + 1), -1);
			if (on && (ahh == tmpH) && (amm == tmpM))
			{
				if (repeat == 1)
				{

					amm += snooze;
					if (amm >= 60)
					{
						ahh++;
						if (ahh >= 24)
						{
							AW++;
							if (AW > 7)
								AW = 1;
							ahh -= 24;
						}
						amm -= 60;
					}

					editor.putInt(String.format("AM%d", i + 1), amm);
					editor.putInt(String.format("AH%d", i + 1), ahh);
					editor.putInt(String.format("AW%d", i + 1), AW);
					editor.commit();

					if (!bBeingAlarm)
					{
						Bundle b = new Bundle();
						b.putInt("AlarmNumber", i + 1);
						Intent activityChangeIntent;
						activityChangeIntent = new Intent(AddAlarmActivity.this, SnoozeActivity.class);

						activityChangeIntent.putExtras(b);
						bBeingAlarm = true;

						// AddAlarmActivity.this.startActivity(activityChangeIntent);

						PendingIntent pi = PendingIntent.getActivity(AddAlarmActivity.this, 0, activityChangeIntent, 0);

						try
						{
							pi.send();
						}
						catch (Exception e)
						{
						}

					}
					else
					{
						editor.putBoolean(String.format("On%d", i + 1), false);
						editor.commit();
					}

				}
				else if (repeat == 2)
				{
					if (AW == day)
					{
						amm += snooze;
						if (amm >= 60)
						{
							ahh++;
							if (ahh >= 24)
							{
								AW++;
								if (AW > 7)
									AW = 1;
								ahh -= 24;
							}
							amm -= 60;
						}

						editor.putInt(String.format("AM%d", i + 1), amm);
						editor.putInt(String.format("AH%d", i + 1), ahh);
						editor.putInt(String.format("AW%d", i + 1), AW);
						editor.commit();
						if (!bBeingAlarm)
						{
							Bundle b = new Bundle();
							b.putInt("AlarmNumber", i + 1);
							Intent activityChangeIntent;
							activityChangeIntent = new Intent(AddAlarmActivity.this, SnoozeActivity.class);
							activityChangeIntent.putExtras(b);
							bBeingAlarm = true;
							// AddAlarmActivity.this.startActivity(activityChangeIntent);

							PendingIntent pi = PendingIntent.getActivity(AddAlarmActivity.this, 0, activityChangeIntent, 0);

							try
							{
								pi.send();
							}
							catch (Exception e)
							{
							}

						}
						else
						{

							int ahh1 = preferences.getInt(String.format("H%d", i + 1), 0);
							int amm1 = preferences.getInt(String.format("M%d", i + 1), 0);
							editor.putInt(String.format("AH%d", i + 1), ahh1);
							editor.putInt(String.format("AM%d", i + 1), amm1);

							Calendar calendar1 = Calendar.getInstance();
							int day1 = calendar1.get(Calendar.DAY_OF_WEEK);
							SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
							String str11 = sdf1.format(new Date());
							String[] tmpStr = str11.split(":");
							int tmpH1 = 0;
							int tmpM1 = 0;
							try
							{
								tmpH1 = Integer.parseInt(tmpStr[0]);
								tmpM1 = Integer.parseInt(tmpStr[1]);
							}
							catch (Exception e)
							{

							}
							if (ahh1 < tmpH1)
							{
								day1++;
							}
							else if (ahh1 == tmpH1)
							{
								if (amm1 <= tmpM1)
								{
									day1++;
								}
							}
							if (day1 > 7)
								day1 = 1;

							int AW1 = -1;

							for (int i1 = day1 - 1; i1 >= 1; i1--)
							{
								boolean bW1 = preferences.getBoolean(String.format("W%d%d", i + 1, i1), false);
								if (bW1)
								{
									AW1 = i;
								}
							}
							for (int i1 = 7; i1 >= day1; i1--)
							{
								boolean bW1 = preferences.getBoolean(String.format("W%d%d", i + 1, i1), false);
								if (bW1)
								{
									AW1 = i;
								}
							}
							editor.putInt(String.format("AW%d", i + 1), AW1);
							editor.commit();

						}

					}
				}
			}
			// String ringtone =
			// preferences.getString(String.format("Ringtone%d",
			// i+1), "Default ringtone");
			// int difficulty =
			// preferences.getInt(String.format("Difficulty%d",
			// i+1), 1);
		}
	}
	
	public void load()
	{
		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;

		int height = h * 2 / 10;
		height = (h - height) / 11;

		int width = w - w * 2 / 10;

		ScrollView scrollView = (ScrollView) findViewById(R.id.addalarmscreenscrollview);

		RelativeLayout layout = (RelativeLayout) findViewById(R.id.addalarmrelativelayout3);

		layout.removeAllViews();
		alarms.clear();

		SharedPreferences preferences = this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		String strKey = "AlarmCnt";

		int n = preferences.getInt(strKey, 0);
		scrollView.getLayoutParams().height = n * height;

		AlarmListener listener = new AlarmListener();
		
		int c = 0;
		for (int i = 0; i < n; i++)
		{
			boolean active = preferences.getBoolean(String.format("Active%d", i + 1), false);
			if(!active) continue;

			Alarm alarm = new Alarm(this, layout, width, height, listener);
			alarm.setId(i + 1);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
			lp.leftMargin = 0;
			lp.topMargin = c * height;
			alarm.setLayoutParams(lp);
			layout.addView(alarm);

			// public void setInfo(int hour, int minute, boolean am, String
			// description)

			String title = preferences.getString(String.format("Title%d", i + 1), "Alarm");
			boolean on = preferences.getBoolean(String.format("On%d", i + 1), false);
			alarm.turnOnOff(on);
			int hh = preferences.getInt(String.format("H%d", i + 1), 0);
			int mm = preferences.getInt(String.format("M%d", i + 1), 0);
			// int ahh = preferences.getInt(String.format("AH%d", i+1), 0);
			// int amm = preferences.getInt(String.format("AM%d", i+1), 0);
			// int repeat = preferences.getInt(String.format("Repeat%d", i+1),
			// 0);
			// int snooze = preferences.getInt(String.format("Snooze%d", i+1),
			// 0);
			// int AW = preferences.getInt(String.format("AW%d", i+1), -1);
			// String ringtone =
			// preferences.getString(String.format("Ringtone%d", i+1),
			// "Default ringtone");
			// int difficulty = preferences.getInt(String.format("Difficulty%d",
			// i+1), 1);
			// String strMsg = preferences.getString(String.format("Message%d",
			// i+1), "Wake me up!");

			alarm.setInfo(hh, mm, title);

			alarms.add(alarm);
			c++;
		}
		scrollView.getLayoutParams().height = c * height;
	}
 
	public class AlarmListener implements AlarmInterface
	{

		@Override
		public void settings(int id)
		{
			wasMinimize = false;
			
			Intent intent = new Intent(AddAlarmActivity.this, SetAlarmActivity.class);
			intent.putExtra("AlarmNumber", id);
			startActivity(intent);
		}

		@Override
		public void delete(int id)
		{
			SharedPreferences preferences = AddAlarmActivity.this.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean(String.format("Active%d", id), false);
			editor.commit();
	
			load();
		}	
		
	}
	
	public interface AlarmInterface
	{
		public void settings(int id);
		public void delete(int id);
	}
}