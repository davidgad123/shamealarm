package com.shameclock1;

import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class Utils
{
	// Constants for which activity calls AddAlarmActivity.onResume()
	public static final int EXTERNAL_ACTIVTY = 0;
	public static final int INTERNAL_ACTIVITY = 1;
	
	// Flag for which activity calls AddAlarmActivity.onResume()
	public static int g_bPreActivityFlag = EXTERNAL_ACTIVTY;

	public static Point getScreenSize(Context context)
	{
		Point size = new Point();
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);
	
		Display d = wm.getDefaultDisplay();
		size.x = metrics.widthPixels;
		size.y = metrics.heightPixels;
		
		/*
		try
		{
			// used when 17 > SDK_INT >= 14; includes window decorations (statusbar bar/menu bar)
			size.x = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
			size.y = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
		}
		catch (Exception ignored)
		{
			try
			{
				// used when SDK_INT >= 17; includes window decorations
				// (statusbar bar/menu bar)
				Point realSize = new Point();
				Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
				size.x = realSize.x;
				size.y = realSize.y;
			}
			catch (Exception ignored2)
			{
			}
		}
		
	*/
		size.x = Math.min(size.x, size.y);
		size.y = Math.max(size.x, size.y);
	
		return size;
	}

	public static int getInt(String str)
	{
		int a = 0;
		try
		{
			a = Integer.valueOf(str);
		}
		catch (Exception e)
		{
		}
		return a;
	}


}
