package com.shameclock1;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class PostmsgActivity extends Activity implements OnClickListener
{

	ArrayList<String>	aryResp;
	ArrayList<String>	aryBrow;
	ArrayList<String>	aryAbso;
	String				strMsg;

	public boolean		bFacebook;
	public boolean		bTwitter;
	public boolean		bHandset;

	public ImageButton	btnResp;
	public ImageButton	btnBrow;
	public ImageButton	btnAbso;
	public ImageButton	btnAdd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_postmsg);
	
		Bundle b = getIntent().getExtras();
	
		if(b != null)
		{
			bFacebook = b.getBoolean("Facebook", false);
			bTwitter = b.getBoolean("Twitter", false);
			bHandset = b.getBoolean("Handset", false);
		}
		
		aryResp = new ArrayList<String>();
	
		try
		{
			InputStream msg = getAssets().open("respectable.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(msg));
			String str;
	
			while((str = in.readLine()) != null)
			{
				aryResp.add(str);
			}
	
			in.close();
		}
		catch (Exception e)
		{
	
		}
	
		aryBrow = new ArrayList<String>();
	
		try
		{
			InputStream msg = getAssets().open("browraising.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(msg));
			String str;
	
			while((str = in.readLine()) != null)
			{
				aryBrow.add(str);
			}
	
			in.close();
		}
		catch (Exception e)
		{
	
		}
	
		aryAbso = new ArrayList<String>();
	
		try
		{
			InputStream msg = getAssets().open("absolutelyfilthy.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(msg));
			String str;
	
			while((str = in.readLine()) != null)
			{
				aryAbso.add(str);
			}
	
			in.close();
		}
		catch (Exception e)
		{
	
		}
		Point size = Utils.getScreenSize(this);
		int w = size.x;
		int h = size.y;
	
		ImageView imgView;
		imgView = (ImageView) findViewById(R.id.postmsgscreentitle);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = w;
		int height = h * 2 / 10;
		
		params.height = height;
		params.setMargins(w / 10, 0, w / 10, 0);
	
		RelativeLayout layout;
		layout = (RelativeLayout) findViewById(R.id.postmsgrelativelayout1);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		params.width = w;
		params.height = h - height;
	
		layout = (RelativeLayout) findViewById(R.id.postmsgrelativelayout2);
		params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
		height = (h - height) / 11;
		params.height = height;
		params.width = w - w * 2 / 10;
		params.setMargins(w / 10, 0, 0, 0);
	
		int width = layout.getLayoutParams().width;
	
		TextView textView = (TextView) findViewById(R.id.postmsgscreentexttitle);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, h / 35);
		params = (RelativeLayout.LayoutParams) textView.getLayoutParams();
		params.width = width;
		params.height = height;
	
		imgView = (ImageView) findViewById(R.id.postmsgscreensplitbar);
		params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
		params.width = width;
		params.height = width / 100;
		params.setMargins(w / 10, 0, 0, 0);
	
		ImageButton imgBtn;
		imgBtn = (ImageButton) findViewById(R.id.respectable);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);
	
    	imgBtn = (ImageButton)findViewById(R.id.respectablecheck);
    	imgBtn.setOnClickListener(this);
        params = (RelativeLayout.LayoutParams)imgBtn.getLayoutParams();
        int tmp = width / 6;
        if (height < tmp) tmp = height;
    	params.width = tmp / 2;
    	params.height = tmp / 2;
    	params.setMargins(w/10 + width - tmp + tmp / 2, height/2 + height / 2 - tmp / 4 , 0, 0);
		
		imgBtn = (ImageButton) findViewById(R.id.browraising);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);
		
    	imgBtn = (ImageButton)findViewById(R.id.browraisingcheck);
    	imgBtn.setOnClickListener(this);
        params = (RelativeLayout.LayoutParams)imgBtn.getLayoutParams();
    	params.width = tmp / 2;
    	params.height = tmp / 2;
    	params.setMargins(w/10 + width - tmp + tmp / 2, height / 2 + height / 2 - tmp / 4, 0, 0);
    	
		
	
		imgBtn = (ImageButton) findViewById(R.id.absolutelyfilthy);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = height;
		params.setMargins(w / 10, height / 2, 0, 0);
	
    	imgBtn = (ImageButton)findViewById(R.id.absolutelyfilthycheck);
    	imgBtn.setOnClickListener(this);
        params = (RelativeLayout.LayoutParams)imgBtn.getLayoutParams();
    	params.width = tmp / 2;
    	params.height = tmp / 2;
    	params.setMargins(w/10 + width - tmp + tmp / 2, height/2 + height/2 - tmp/4, 0, 0);
    	
		
		imgBtn = (ImageButton) findViewById(R.id.addyourown);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = height;
		params.setMargins(w / 10, (height*3)/2, 0, 0);
	
    	imgBtn = (ImageButton)findViewById(R.id.addyourowncheck);
    	imgBtn.setOnClickListener(this);
        params = (RelativeLayout.LayoutParams)imgBtn.getLayoutParams();
    	params.width = tmp / 2;
    	params.height = tmp / 2;
    	params.setMargins(w/10 + width - tmp + tmp / 2, (height*3)/2 + height/2 - tmp/4, 0, 0);
    	

    	imgBtn = (ImageButton) findViewById(R.id.postmsgnext);
		imgBtn.setOnClickListener(this);
		params = (RelativeLayout.LayoutParams) imgBtn.getLayoutParams();
		params.width = width * 5 / 6;
		params.height = height;
		params.setMargins(w / 10, height, 0, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onClick(View v)
	{
	
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(PostmsgActivity.this);
		builderSingle.setCancelable(false);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("Select One Message");
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PostmsgActivity.this, android.R.layout.select_dialog_singlechoice);
		
		switch (v.getId())
		{
			case R.id.respectable:				
				chooseShame(0);
				break;
			case R.id.browraising:
				chooseShame(1);
				break;
			case R.id.absolutelyfilthy:
				chooseShame(2);
				break;
			case R.id.addyourown:
				AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(PostmsgActivity.this);
				dlgBuilder.setCancelable(false);
				dlgBuilder.setIcon(R.drawable.ic_launcher);
				dlgBuilder.setTitle("Enter Your Message");
				final EditText input = new EditText(PostmsgActivity.this);
				
				dlgBuilder.setView(input);
				dlgBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int whichButton)
					{
						strMsg = input.getText().toString();
	
						checkButton(3);
					}
				});
				dlgBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int whichButton)
					{
	
					}
				});
	
				dlgBuilder.show();
				break;
			case R.id.postmsgnext:
				Bundle b = new Bundle();
				b.putString("PostMessage", strMsg);
				b.putBoolean("Facebook", bFacebook);
				b.putBoolean("Twitter", bTwitter);
				b.putBoolean("Handset", bHandset);
				Intent activityChangeIntent;
				activityChangeIntent = new Intent(PostmsgActivity.this, SetAlarmActivity.class);
				activityChangeIntent.putExtras(b);
				PostmsgActivity.this.startActivity(activityChangeIntent);
				finish();
				break;
			default:
				break;
		}
	}

	public void checkButton(int index)
	{
    	ImageButton imgBtn;
    	
    	imgBtn = (ImageButton)findViewById(R.id.respectablecheck);
    	imgBtn.setImageResource(index == 0 ? R.drawable.connectchecked : R.drawable.connectunchecked); 
		
    	imgBtn = (ImageButton)findViewById(R.id.browraisingcheck);
    	imgBtn.setImageResource(index == 1 ? R.drawable.connectchecked : R.drawable.connectunchecked); 
		
    	imgBtn = (ImageButton)findViewById(R.id.absolutelyfilthycheck);
    	imgBtn.setImageResource(index == 2 ? R.drawable.connectchecked : R.drawable.connectunchecked); 
		
    	imgBtn = (ImageButton)findViewById(R.id.addyourowncheck);
    	imgBtn.setImageResource(index == 3 ? R.drawable.connectchecked : R.drawable.connectunchecked); 		
	}

	public void chooseShame(final int index)
	{
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(PostmsgActivity.this);
		builderSingle.setCancelable(false);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("Select");
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PostmsgActivity.this, android.R.layout.select_dialog_singlechoice);
		
		arrayAdapter.add("Choose Randomly");
		arrayAdapter.add("Select my Shame");

		builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch(index)
				{
					case 0:
						doRespectable(which == 1);
						break;
					case 1:
						doBrowraising(which == 1);
						break;
					case 2:
						doAbsolutely(which == 1);
						break;
				}
			}
		});
		builderSingle.show();				
	}
	
	public void doRespectable(boolean select)
	{
		if(!select)
		{
			int index = randomBox(aryResp.size());
			strMsg = aryResp.get(index);
			
			checkButton(0);

			return;
		}

		AlertDialog.Builder builderSingle = new AlertDialog.Builder(PostmsgActivity.this);
		builderSingle.setCancelable(false);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("Select One Message");
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PostmsgActivity.this, android.R.layout.select_dialog_singlechoice);
		
		for (int i = 0; i < aryResp.size(); i++)
		{
			arrayAdapter.add(aryResp.get(i));
		}

		builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String strName = arrayAdapter.getItem(which);
				strMsg = strName;
				
				showSelected(0);
			}
		});
		builderSingle.show();		
	}
	
	public void doBrowraising(boolean select)
	{
		if(!select)
		{
			int index = randomBox(aryBrow.size());
			strMsg = aryBrow.get(index);

			checkButton(1);
			return;
		}

		AlertDialog.Builder builderSingle = new AlertDialog.Builder(PostmsgActivity.this);
		builderSingle.setCancelable(false);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("Select One Message");
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PostmsgActivity.this, android.R.layout.select_dialog_singlechoice);

		for (int i = 0; i < aryBrow.size(); i++)
		{
			arrayAdapter.add(aryBrow.get(i));
		}
		builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String strName = arrayAdapter.getItem(which);
				strMsg = strName;
				showSelected(1);
			}
		});
		builderSingle.show();		
	}
	
	public void doAbsolutely(boolean select)
	{
		if(!select)
		{
			int index = randomBox(aryAbso.size());
			strMsg = aryAbso.get(index);
			
			checkButton(2);
			return;
		}


		AlertDialog.Builder builderSingle = new AlertDialog.Builder(PostmsgActivity.this);
		builderSingle.setCancelable(false);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("Select One Message");
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PostmsgActivity.this, android.R.layout.select_dialog_singlechoice);

		
		for (int i = 0; i < aryAbso.size(); i++)
		{
			arrayAdapter.add(aryAbso.get(i));
		}

		builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String strName = arrayAdapter.getItem(which);
				strMsg = strName;
				
				showSelected(2);
			}
		});
		builderSingle.show();		
	}
	
	public void showSelected(final int index)
	{
		AlertDialog.Builder builderInner = new AlertDialog.Builder(PostmsgActivity.this);
		builderInner.setCancelable(false);
		builderInner.setMessage(strMsg);
		builderInner.setTitle("Your Selected Message is");
		builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				checkButton(index);
				
				dialog.dismiss();
			}
		});
		builderInner.show();				
	}

	public int randomBox(int size) 
	{
	    Random rand = new Random();
	    int pickedNumber = rand.nextInt(size);
	    return pickedNumber;
	}
}