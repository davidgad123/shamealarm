package com.shameclock1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

@SuppressLint("NewApi")
public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		
        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);

		Point size = Utils.getScreenSize(this);        
        int w = size.x;
        int h = size.y;
		
        
        ImageView imgView;
    	imgView = (ImageView)findViewById(R.id.loginscreentitle);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)imgView.getLayoutParams();
    	params.width = w;
    	int height = h*2/10;
    	params.height = height;
    	params.setMargins(w/10, 0, w/10, 0);
    	    	
    	RelativeLayout layout;
    	layout = (RelativeLayout)findViewById(R.id.loginrelativelayout1);
    	params = (RelativeLayout.LayoutParams)layout.getLayoutParams();
    	params.width = w;
    	params.height = h-height;
    	
    	layout = (RelativeLayout)findViewById(R.id.loginrelativelayout2);
    	params = (RelativeLayout.LayoutParams)layout.getLayoutParams();
    	height = (h-height) / 11;
    	params.height = height;
    	params.width = w - w*2/10;
    	params.setMargins(w/10, 0, 0, 0);

    	imgView = (ImageView)findViewById(R.id.loginscreenlogin);
        params = (RelativeLayout.LayoutParams)imgView.getLayoutParams();
    	params.width = (int)(height*3*0.53);
    	params.height = height;
    	
    	int width = layout.getLayoutParams().width;
        imgView = (ImageView)findViewById(R.id.loginscreensplitbar);
        params = (RelativeLayout.LayoutParams)imgView.getLayoutParams();
    	params.width = width;
    	params.height = width / 100;
    	params.setMargins(w/10, 0, 0, 0);
    	
    	EditText editText;
    	editText = (EditText)findViewById(R.id.loginscreenusername);
    	editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, height*2/3);
        params = (RelativeLayout.LayoutParams)editText.getLayoutParams();
    	params.width = width * 5 / 6;
    	params.height = height;
    	params.setMargins(w/10, height/2, 0, 0);
    	
    	editText = (EditText)findViewById(R.id.loginscreenpassword);
    	editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, height*2/3);
        params = (RelativeLayout.LayoutParams)editText.getLayoutParams();
    	params.width = width * 5 / 6;
    	params.height = height;
    	params.setMargins(w/10, height/2, 0, 0);
    	
    	ImageButton imgBtn;
    	imgBtn = (ImageButton)findViewById(R.id.loginscreenloginbtn);
        params = (RelativeLayout.LayoutParams)imgBtn.getLayoutParams();
    	params.width = width * 5 / 6;
    	params.height = (int)(width * 5 / 4.32 / 6);
    	params.setMargins(w/10, height/2, 0, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}