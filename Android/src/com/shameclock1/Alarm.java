package com.shameclock1;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.shameclock1.AddAlarmActivity.AlarmInterface;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class Alarm extends RelativeLayout implements OnClickListener
{

	public int			iHour;
	public int			iMinute;

	public String		strTitle; 

	public boolean		bChecked;

	public TextView		txtTime;
	public TextView		txtDesc;
	public ImageButton	btnCheck;
	public ImageView	imgSplit;

	SharedPreferences	preferences;

	public Context		myContext;

	public int			width;
	public int			height;
	public RelativeLayout layout;
	public AlarmInterface listener;
	
	public Alarm(Context context)
	{
		super(context);
	}

	public Alarm(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public Alarm(Context context, RelativeLayout layout, int width, int height, AlarmInterface listener)
	{
		super(context);

		this.listener = listener;
		this.layout = layout;
		this.width = width;
		this.height = height;

		myContext = context;

		preferences = context.getSharedPreferences("ShameClockPreferences", Context.MODE_PRIVATE);

		setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				openOptions();
			}			
		});

		txtTime = new TextView(context);
		txtTime.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		txtTime.setId(1);
		txtTime.setTextColor(0xFFFFFFFF);
		this.addView(txtTime);

		txtDesc = new TextView(context);
		txtDesc.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		txtDesc.setId(2);
		txtDesc.setTextColor(0xFF00A7DE);
		this.addView(txtDesc);

		btnCheck = new ImageButton(context);
		btnCheck.setBackgroundColor(Color.TRANSPARENT);
		btnCheck.setImageResource(getResources().getIdentifier("alarmchecked", "drawable", context.getPackageName()));
		btnCheck.setPadding(0, 0, 0, 0);
		btnCheck.setScaleType(ScaleType.FIT_CENTER);
		btnCheck.setId(3);
		btnCheck.setOnClickListener(this);
		this.addView(btnCheck);

		imgSplit = new ImageView(context);
		imgSplit.setScaleType(ImageView.ScaleType.FIT_XY);
		imgSplit.setImageResource(getResources().getIdentifier("splitbar1", "drawable", context.getPackageName()));
		imgSplit.setId(4);
		this.addView(imgSplit);

		int sh = height / 20;
		txtTime.getLayoutParams().width = width / 2;
		txtTime.getLayoutParams().height = (height - sh) / 2;
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) txtTime.getLayoutParams();
		params.setMargins(0, 0, 0, 0);
		txtTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, (height - sh) * 2 / 5);
		// txtTime.setLayoutParams(params);

		txtDesc.getLayoutParams().width = width / 2;
		txtDesc.getLayoutParams().height = (height - sh) / 2;
		params = (RelativeLayout.LayoutParams) txtDesc.getLayoutParams();
		params.setMargins(0, (height - sh) / 2, 0, 0);
		txtDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, (height - sh) * 2 / 5);

		int w = (height - sh) / 2;
		btnCheck.getLayoutParams().width = w;
		btnCheck.getLayoutParams().height = w;
		params = (RelativeLayout.LayoutParams) btnCheck.getLayoutParams();
		params.setMargins(width - w, (height - sh) / 2 - w / 2, 0, 0);

		imgSplit.getLayoutParams().width = width;
		imgSplit.getLayoutParams().height = sh;
		params = (RelativeLayout.LayoutParams) imgSplit.getLayoutParams();
		params.setMargins(0, height - sh, 0, 0);
	}

	public Alarm(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public void onClick(View v)
	{
		switch (v.getId())
		{
			case 3:
				bChecked = !bChecked;

				SharedPreferences.Editor editor = preferences.edit();
				String strKey = String.format("On%d", this.getId());
				editor.putBoolean(strKey, bChecked);

				int repeat = preferences.getInt(String.format("Repeat%d", this.getId()), 0);

				if (bChecked)
				{
					int ahh = preferences.getInt(String.format("H%d", this.getId()), 0);
					int amm = preferences.getInt(String.format("M%d", this.getId()), 0);
					editor.putInt(String.format("AH%d", this.getId()), ahh);
					editor.putInt(String.format("AM%d", this.getId()), amm);

					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
					String str1 = sdf.format(new Date());
					String[] tmpStr = str1.split(":");
					int tmpH = 0;
					int tmpM = 0;
					try
					{
						tmpH = Integer.parseInt(tmpStr[0]);
						tmpM = Integer.parseInt(tmpStr[1]);
					}
					catch (Exception e)
					{

					}

					Calendar calendar = Calendar.getInstance();
					int day = calendar.get(Calendar.DAY_OF_WEEK);
					if (ahh < tmpH)
					{
						day++;
					}
					else if (ahh == tmpH)
					{
						if (amm <= tmpM)
						{
							day++;
						}
					}
					if (day > 7)
						day = 1;

					int AW = -1;

					for (int i = day - 1; i >= 1; i--)
					{
						boolean bW = preferences.getBoolean(String.format("W%d%d", this.getId(), i), false);
						if (bW)
						{
							AW = i;
						}
					}
					for (int i = 7; i >= day; i--)
					{
						boolean bW = preferences.getBoolean(String.format("W%d%d", this.getId(), i), false);
						if (bW)
						{
							AW = i;
						}
					}

					editor.putInt(String.format("AW%d", this.getId()), AW);

					editor.commit();
				}

				editor.commit();

				this.turnOnOff(bChecked);
				break;
			default:
				break;
		}
	}

	public void setInfo(int hour, int minute, String title)
	{

		iHour = hour;
		iMinute = minute;
		strTitle = title;

		String am = "AM";
		int tmpHour = iHour;
		if (iHour >= 12)
		{
			am = "PM";
			if (tmpHour > 12)
				tmpHour -= 12;
		}

		txtTime.setText(String.format("%02d:%02d ", tmpHour, iMinute) + am);
		txtDesc.setText(strTitle);
	}

	public void turnOnOff(boolean on)
	{

		bChecked = on;
		if (bChecked)
		{
			btnCheck.setImageResource(getResources().getIdentifier("alarmchecked", "drawable", myContext.getPackageName()));
		}
		else
		{
			btnCheck.setImageResource(getResources().getIdentifier("alarmunchecked", "drawable", myContext.getPackageName()));
		}
	}

	public void openOptions()
	{
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(myContext);
		builderSingle.setCancelable(false);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("Alarm Options");
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(myContext, android.R.layout.select_dialog_singlechoice);
		
		arrayAdapter.add("Change Settings");
		arrayAdapter.add("Delete");

		builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch(which)
				{
					case 0:
						if(listener != null) listener.settings(Alarm.this.getId());
						break;
					case 1:
						askDelete(Alarm.this.getId());
						break;
				}
			}
		});
		builderSingle.show();						
	}

	private void askDelete(final int id)
	{
		AlertDialog.Builder builderInner = new AlertDialog.Builder(myContext);
		builderInner.setCancelable(false);
		builderInner.setMessage("Are you sure you want to delete this alarm?");
//		builderInner.setTitle("Your Selected Message is");
		builderInner.setPositiveButton("Yes", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				deleteAlarm(id);
				
				dialog.dismiss();
			}
		});		
		builderInner.setNegativeButton("No", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		builderInner.show();		
	}
	
	private void deleteAlarm(int id)
	{
		layout.removeView(Alarm.this);
		
		if(listener != null) listener.delete(Alarm.this.getId());
	}
}